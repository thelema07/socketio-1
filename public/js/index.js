var socket = io('https://serene-castle-19164.herokuapp.com/');
// https://serene-castle-19164.herokuapp.com/
// localhost:3000

socket.on('connect', function () {
  console.log('Connected to server');
  socket.emit('newUser',{
    new:'New User Connected',
    date: new Date().getTime()
  });
  let li = $('<li></li>');
  li.text('Welcome to the Chat Application');

  $('#chatQueue').append(li);
});

socket.on('disconnect', function () {
  console.log('Disconnected from server');
});

$('#chatBody').on('submit',(e)=>{
  e.preventDefault();
  socket.emit('sendMessage',{
    user: 'DannyRojo',
    text: $('[name=message]').val(),
    date: new Date().getDate()
  },() => {
    console.log('From server to you');
  });
  $('[name=message]').val('');
});

socket.on('userJoined',(data) => {
  let li = $('<li></li>');
  li.text(data.message);

  $('#chatQueue').append(li);
});

socket.on('newMessage',(data) => {
  console.log(data);
  let li = $('<li></li>');
  li.text(data.msg);

  $('#chatQueue').append(li);
});

